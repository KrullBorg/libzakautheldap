/*
 * Copyright (C) 2005-2010 Andrea Zagli <azagli@libero.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
	#include "config.h"
#endif
  
#include <gtk/gtk.h>
#include <ldap.h>

#ifdef HAVE_LIBCONFI
	#include <libconfi.h>
#endif

static GtkWidget *txt_utente;
static GtkWidget *txt_password;
static GtkWidget *exp_cambio;
static GtkWidget *txt_password_nuova;
static GtkWidget *txt_password_conferma;

/* PRIVATE */
#ifdef HAVE_LIBCONFI
static gboolean
get_connection_parameters_from_confi (Confi *confi, gchar **host, gchar **base_dn, gchar **user_field)
{
	gboolean ret = TRUE;

	*host = confi_path_get_value (confi, "aute/aute-ldap/ldap/host");
	*base_dn = confi_path_get_value (confi, "aute/aute-ldap/ldap/base_dn");
	*user_field = confi_path_get_value (confi, "aute/aute-ldap/ldap/user_field");

	if (*host == NULL
	    || strcmp (g_strstrip (*host), "") == 0
	    || base_dn == NULL
	    || strcmp (g_strstrip (*base_dn), "") == 0
	    || user_field == NULL
	    || strcmp (g_strstrip (*user_field), "") == 0)
		{
			*host = NULL;
			*base_dn = NULL;
			*user_field = NULL;

			ret = FALSE;
		}

	return ret;
}
#endif

static gchar
*controllo (GSList *parameters)
{
	gchar *user_dn;
	gchar *utente;
	gchar *password;
	gchar *password_nuova;

	gchar *host;
	gchar *base_dn;
	gchar *user_field;

	LDAP *ldap;
	int version;
	int retldap;

	user_dn = "";

	utente = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_utente))));
	password = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password))));

	host = NULL;
	base_dn = NULL;
	user_field = NULL;

#ifdef HAVE_LIBCONFI
	/* the first and only parameters must be a Confi object */
	/* leggo i parametri di connessione dalla configurazione */
	if (IS_CONFI (parameters->data))
		{
			if (!get_connection_parameters_from_confi (CONFI (parameters->data), &host, &base_dn, &user_field))
				{
					host = NULL;
					base_dn = NULL;
					user_field = NULL;
				}
		}
#endif

	if (host == NULL)
		{
			GSList *param;

			param = g_slist_next (parameters);
			if (param != NULL && param->data != NULL)
				{
					host = g_strdup ((gchar *)param->data);
					host = g_strstrip (host);
					if (g_strcmp0 (host, "") == 0)
						{
							host = NULL;
						}
					else
						{
							param = g_slist_next (param);
							if (param != NULL && param->data != NULL)
								{
									base_dn = g_strdup ((gchar *)param->data);
									base_dn = g_strstrip (base_dn);
									if (g_strcmp0 (base_dn, "") == 0)
										{
											base_dn = NULL;
										}
									else
										{
											param = g_slist_next (param);
											if (param != NULL && param->data != NULL)
												{
													user_field = g_strdup ((gchar *)param->data);
													user_field = g_strstrip (user_field);
													if (g_strcmp0 (user_field, "") == 0)
														{
															user_field = NULL;
														}
												}
										}
								}
						}
				}
		}

	if (host == NULL
		|| base_dn == NULL
		|| user_field == NULL)
		{
			return NULL;
		}
	ldap = NULL;
	version = 3;

	retldap = ldap_initialize (&ldap, host);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nell'inizializzazione.\n%s\n", ldap_err2string (retldap));
			return NULL;
		}

	retldap = ldap_set_option (ldap, LDAP_OPT_PROTOCOL_VERSION, &version);
	if (retldap != LDAP_OPT_SUCCESS)
		{
			g_warning ("Errore nell'impostazione della versione del protocollo.\n%s\n", ldap_err2string (retldap));
			return NULL;
		}

	user_dn = g_strdup_printf ("%s=%s,%s", user_field, utente, base_dn);
	retldap = ldap_simple_bind_s (ldap, user_dn, password);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nel bind.\n%s\n", ldap_err2string (retldap));
			return NULL;
		}

/*
	if (strcmp (utente, "") != 0
	    && gtk_expander_get_expanded (GTK_EXPANDER (exp_cambio)))
		{
			password_nuova = g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password_nuova))));
			if (strlen (password_nuova) == 0 || strcmp (g_strstrip (password_nuova), "") == 0)
				{
					g_warning ("La nuova password è vuota.");
				}
			else if (strcmp (g_strstrip (password_nuova), g_strstrip (g_strdup (gtk_entry_get_text (GTK_ENTRY (txt_password_conferma))))) != 0)
				{
					g_warning ("La nuova password e la conferma non coincidono.");
				}
			else
				{
					sql = g_strdup_printf ("UPDATE utenti "
				                         "SET password = '%s' "
				                         "WHERE codice = '%s'",
				                         gdaex_strescape (cifra_password (password_nuova), NULL),
				                         gdaex_strescape (utente, NULL));
					if (gdaex_execute (gdaex, sql) == -1)
						{
							g_warning ("Errore durante la modifica della password.");
							return NULL;
						}
				}
		}
*/

	retldap = ldap_unbind (ldap);
	if (retldap != LDAP_SUCCESS)
		{
			g_warning ("Errore nell'unbind.\n%s\n", ldap_err2string (retldap));
			return NULL;
		}

	return user_dn;
}

/* PUBLIC */
gchar
*autentica (GSList *parameters)
{
	GError *error;
	gchar *ret = NULL;

	error = NULL;

	GtkBuilder *gtkbuilder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (gtkbuilder, GUIDIR "/auteldap.gui", &error))
		{
			return NULL;
		}

	GtkWidget *diag = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "diag_main"));

	txt_utente = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_utente"));
	txt_password = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password"));
	exp_cambio = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "exp_cambio"));
	txt_password_nuova = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password_nuova"));
	txt_password_conferma = GTK_WIDGET (gtk_builder_get_object (gtkbuilder, "txt_password_conferma"));

	/* imposto di default l'utente corrente della sessione */
	gtk_entry_set_text (GTK_ENTRY (txt_utente), g_get_user_name ());
	gtk_editable_select_region (GTK_EDITABLE (txt_utente), 0, -1);

	switch (gtk_dialog_run (GTK_DIALOG (diag)))
		{
			case GTK_RESPONSE_OK:
				/* controllo dell'utente e della password */
				ret = controllo (parameters);
				break;

			case GTK_RESPONSE_CANCEL:
			case GTK_RESPONSE_NONE:
			case GTK_RESPONSE_DELETE_EVENT:
				ret = g_strdup ("");
				break;

			default:
				break;
		}

	gtk_widget_destroy (diag);
	g_object_unref (gtkbuilder);

	return ret;
}

/**
 * crea_utente:
 * @parameters:
 * @codice:
 * @password:
 */
gboolean
crea_utente (GSList *parameters, const gchar *codice, const gchar *password)
{
	/* TODO */
	return FALSE;
}

/**
 * modifice_utente:
 * @parameters:
 * @codice:
 * @password:
 */
gboolean
modifica_utente (GSList *parameters, const gchar *codice, const gchar *password)
{
	/* TODO */
	return FALSE;
}

/**
 * elimina_utente:
 * @parameters:
 * @codice:
 */
gboolean
elimina_utente (GSList *parameters, const gchar *codice)
{
	/* TODO */
	return FALSE;
}
